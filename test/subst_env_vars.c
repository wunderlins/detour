#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "CUnit/Basic.h"

void test_no_vars() {
    char *test_str = "no variables in this string";
    envreplace_t *ret = subst_env_vars(test_str);
    CU_ASSERT_EQUAL(ret->found, 0);
    CU_ASSERT_EQUAL(strcmp(ret->result, test_str), 0);
}

void test_find_two() {
    char *test_str = "windir: ${windir}, var one: ${one} and var two: ${two}, OS: ${OS}";
    envreplace_t *ret = subst_env_vars(test_str);
    CU_ASSERT_EQUAL(ret->found, 4);
    CU_ASSERT_EQUAL(ret->error, 0);
    CU_ASSERT_EQUAL(ret->replaced, 2);
    CU_ASSERT_EQUAL(ret->unreplaced, 2);
}

void test_unclosed() {
    char *test_str = "windir: ${windir";
    envreplace_t *ret = subst_env_vars(test_str);
    CU_ASSERT_EQUAL(ret->error, 1);

    test_str = "${windir";
    ret = subst_env_vars(test_str);
    CU_ASSERT_EQUAL(ret->error, 1);
}

int main(int argc, char *argv[]) {
    // Initialize the CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    // Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
    // Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
    CU_basic_set_mode(CU_BRM_VERBOSE);

    CU_pSuite pSuite = NULL;

    // Add a suite to the registry
    pSuite = CU_add_suite("subst_env_vars", 0, 0);
    // Always check if add was successful
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // test all error conditions of set_addr_from_string()
    if (NULL == CU_add_test(pSuite, "test_no_vars", test_no_vars)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // test all error conditions of set_addr_from_string()
    if (NULL == CU_add_test(pSuite, "test_find_two", test_find_two)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    // Run the tests and show the run summary
    CU_basic_run_tests();
    return CU_get_number_of_tests_failed();
}
