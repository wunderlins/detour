/**
 * @file util.h
 * @author Simon Wunderlin <swunderlin@gmail.com>
 * @version 0.1
 * @date 2022-05-07
 * @brief Utility functions
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef __UTIL_H__
#define __UTIL_H__

#ifdef _WIN32
	#ifndef WIN32_LEAN_AND_MEAN
	#	define WIN32_LEAN_AND_MEAN
	#endif
	#include <windows.h>
#else
	#include <linux/limits.h>
#endif
#include <stdio.h>

#include <stdarg.h>
#include <stddef.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "ini.h"

typedef struct match_t match_t; 
/**
 * @brief variable replacement information
 * 
 * This struct holds positional information about 
 * variables that must be replaced within a string.
 * 
 * This struct may be used a singly linked list.
 * 
 */
typedef struct match_t {
	/// start position of the variable name `$`
	int start;
	/// length including ${...} of the whole place holder
	int length;
	/// name of the variable (between `${` .. and .. `}`)
	char *name;
	/// the value resolved by `gentnv()` or NULL if unset
	char *value;
	/// pointer to the next variable or NULL if end.
	match_t *next;
} match_t;

/**
 * @brief state of variable subsitution replacement
 * 
 * always check for `error`. If it is no 0, then a parsing error happened 
 * and the data is not to be trusted.
 */
typedef struct envreplace_t {
	/// how many variable keywords were found
	int found;
	/// how many variable names could be replaced with environment variable's content
	int replaced;
	/// how many variables were replaced by '' because there was no env
	int unreplaced;
	/// parsing error code, 0 means success.
	int error;
	/// This is the result string with all variable placeholders replaced.
	char *result;
} envreplace_t;

/**
 * @brief open a os specific log file in the user context
 * 
 * @return char* 
 */
char *open_log();

/**
 * @brief make sure log file does not grow indefintiely
 * 
 * @param size 
 */
void prune_log_file(size_t size);

/**
 * @brief replace all backslashes with forward shlashes
 * 
 * @param [in, out] path 
 */
void fwdslash(char *path);

/**
 * @brief remove the file name from a path
 * 
 * The function will place a `\0` over the last '/'. The input 
 * string is modified.
 * 
 * @param [in, out] path 
 */
void dirname(char *path);

/**
 * @brief parse ini file
 * 
 * @param [in] inifile String path to ini file
 * @return ini_section_list_t* 
 */
ini_section_list_t* read_ini(char *inifile);

/**
 * @brief run an excecuable
 * 
 * This function runs an executable detached fro mthis process.
 * 
 * The only implementation available for windows is 
 * with `CreateProcess`.
 * 
 * @param [in] args array of command line arguments
 * @param [in] sect active configuration section.
 */
void run(char **args, ini_section_t *sect);

/**
 * @brief Display error message
 * 
 * On windows, a dialog is shown.
 * 
 * @param [in] text 
 */
void error(char* text);

/**
 * @brief replace ${...} with environment variables
 * 
 * This function replaces all placeholders that are enclosed in `${...}`
 * with it's equivalent environment variable.
 * 
 * @note: The search for environment variables is case sensitive.
 * @param [in] string string containign variable place holder.
 * @return envreplace_t* 
 */
envreplace_t* subst_env_vars(char *string);

// https://stackoverflow.com/questions/6508461/logging-library-for-c
void logger(const char* tag, const char* message);

#endif // __UTIL_H__