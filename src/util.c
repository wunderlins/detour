#include "util.h"

static FILE *logfp = NULL;
static FILE *logfp2 = NULL;
static char *logfile = NULL;
static char *logfile2 = NULL;

char *open_log() {
	char path[PATH_MAX] = {0};

#ifdef _WIN32
	strcat(path, getenv("LOCALAPPDATA"));
	if (path == NULL) {
		strcat(path, getenv("APPDATA"));
		if (path != NULL)
			strcat(path, "\\..\\local");
	}
#else
	strcat(path, getenv("HOME"));
#endif

	if (path == NULL)
		strcat(path, "/tmp");
	
	logfile = (char*) malloc(PATH_MAX+1);
	logfile2 = (char*) malloc(PATH_MAX+1);

	strcpy(logfile2, path);
	strcat(path, "/detour.log");
	strcat(logfile2, "/detour.tmp");

	int ok;
#ifdef __MINGW32__
	ok = (_fullpath(logfile, path, PATH_MAX) != NULL);
#else
	ok = (realpath(path, logfile) != NULL);
#endif

	if (!ok) return NULL;
	logfp = fopen(logfile, "ab");
	if (logfp == NULL) return NULL;
	
	return logfile;
}

void prune_log_file(size_t size) {

	// file length, we expect the pointer to be at the end, since we append.
	size_t length = ftell(logfp);
	char msg[100];

	// nothing to do
	if (length < size)
		return;

	//sprintf(msg, "%lld", length);
	//logger("LENGTH", msg);

	// open new file
	fclose(logfp);

	logfp = fopen(logfile, "rb");
	logfp2 = fopen(logfile2, "wb");

	// find start
	fseek(logfp, length - size, SEEK_SET);
	char c;
	while((c = fgetc(logfp)) != '\n' && c != EOF); // find next newline
	
	// copy reminder
	while ((c = fgetc(logfp)) != EOF)
		fputc(c, logfp2);
	
	fclose(logfp);
	fclose(logfp2);
	
	// delete original file, rename new file
	unlink(logfile);
	rename((const char*) logfile2, (const char*) logfile);
}

void fwdslash(char *path) {
	int l = strlen(path);
	while (l-- >= 0) {
		if (path[l] == '\\')
			path[l] = '/';
	}
}

void dirname(char *path) {
	int l = strlen(path);
	while (l-- >= 0) {
		if (path[l] == '/')
			break;
		path[l] = 0;
	}
}

ini_section_list_t* read_ini(char *inifile) {
	int errnum = 0;
	FILE *fp;
	
	// try to open ini file
	fp = fopen(inifile, "rb");
	if (fp == NULL) {

		errnum = errno;
		char *msg = NULL;
		sprintf(msg, "Error opening file: '%s', %d, %s\n", inifile, 
		        errnum, strerror(errnum));
		error(msg);
		
		return NULL;
	} 
	
	ini_section_list_t *ini = ini_parse(fp);
	fclose(fp);

	return ini;
}

/**
 * Windows window visibility
 * 
 * https://msdn.microsoft.com/en-us/library/windows/desktop/ms633548(v=vs.85).aspx
 * 
 * SW_FORCEMINIMIZE 11
 * 	Minimizes a window, even if the thread that owns the window is not 
 * 	responding. This flag should only be used when minimizing windows 
 * 	from a different thread.
 * 
 * SW_HIDE 0
 * 	Hides the window and activates another window.
 * 
 * SW_MAXIMIZE 3
 * 	Maximizes the specified window.
 * 
 * SW_MINIMIZE 6
 * 	Minimizes the specified window and activates the next top-level 
 * 	window in the Z order.
 * 
 * SW_RESTORE 9
 * 	Activates and displays the window. If the window is minimized or 
 * 	maximized, the system restores it to its original size and 
 * 	position. An application should specify this flag when 
 * 	restoring a minimized window.
 * 
 * SW_SHOW 5
 * 	Activates the window and displays it in its current size 
 * 	and position.
 * 
 * SW_SHOWDEFAULT 10
 * 	Sets the show state based on the SW_ value specified in the 
 * 	STARTUPINFO structure passed to the CreateProcess function 
 * 	by the program that started the application.
 * 
 * SW_SHOWMAXIMIZED 3
 * 	Activates the window and displays it as a maximized window.
 * 
 * SW_SHOWMINIMIZED 2
 * 	Activates the window and displays it as a minimized window.
 * 
 * SW_SHOWMINNOACTIVE 7
 * 	Displays the window as a minimized window. This value is 
 * 	similar to SW_SHOWMINIMIZED, except the window is not activated.
 * 
 * SW_SHOWNA 8
 * 	Displays the window in its current size and position. This 
 * 	value is similar to SW_SHOW, except that the window is not 
 * 	activated.
 * 
 * SW_SHOWNOACTIVATE 4
 * 	Displays a window in its most recent size and position. This 
 * 	value is similar to SW_SHOWNORMAL, except that the window is 
 * 	not activated.
 * 
 * SW_SHOWNORMAL 1
 * 	Activates and displays a window. If the window is minimized 
 * 	or maximized, the system restores it to its original size 
 * 	and position. An application should specify this flag when 
 * 	displaying the window for the first time.
 */

void run(char **args, ini_section_t *sect) {
	char *cwd = NULL;

#ifdef _WIN32
	BOOL ret;
	PROCESS_INFORMATION pi;
	STARTUPINFO si = {sizeof si};
	si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES; 
	si.wShowWindow = SW_SHOWNORMAL; // SW_SHOWMINNOACTIVE SW_HIDE;

	char cmd[2048] = {0};
	int i = 0;
	while(args[i] != NULL) {
		//if (i == 0) // quote path to executable
		strcat(cmd, "\"");
		strcat(cmd, args[i++]);
		//if (i == 0) // quote path to executable
		strcat(cmd, "\"");
		strcat(cmd, " ");
	}
	
	logger("RUN", cmd);

	// check if we need to show some window features
	for (int i=0; i<sect->length; i++) {
		// window features
		if (strcmp(sect->items[i]->name, "hidden") == 0) {
			char *lValue = strdup(sect->items[i]->value);
			for (i=0; i<strlen(lValue); i++)
				lValue[i] = tolower(lValue[i]);
			
			if (strcmp(lValue, "true") == 0) {
				//error("hide");
				si.wShowWindow = SW_HIDE;
			}
			continue;
		}

		if (sect->items[i]->name && strcmp(sect->items[i]->name, "cwd") == 0 && 
			sect->items[i]->value != NULL &&
			strcmp(sect->items[i]->value, "") != 0
		) {
			envreplace_t *ret = subst_env_vars(sect->items[i]->value);
			cwd = ret->result;
		}
	}
	//error(cwd);
	ret = CreateProcess(NULL, cmd, 0, 0, FALSE, NORMAL_PRIORITY_CLASS, NULL, cwd, &si, &pi);
#else
	// FIXME: double fork execution for posix systems
	fprintf(stderr, "Not implemented");
	logger("RUN",  "Not implemented");
#endif
}

void error(char *text) {
	fprintf(stderr, text);
	logger("ERROR", text);
#ifdef _WIN32
	MessageBox(NULL, text, "Detour Error", MB_OK | MB_ICONERROR);
#endif
}

envreplace_t* subst_env_vars(char *string) {
	int i=0;

	envreplace_t *res = malloc(sizeof(envreplace_t));
	res->found = 0;
	res->replaced = 0;
	res->unreplaced = 0;
	res->error = 0;
	res->result = NULL;

	int len_in = strlen(string);

	// head of the linked list
	match_t *root = malloc(sizeof(match_t));
	root->start  = -1;
	root->length = -1;
	root->next   = NULL;
	match_t *last = root; // tail of the linked list

	int len_out  = 0;
	int count    = 0;
	int open     = -1;

	// 1. find all place holders, scan for ${...} and remember positions
	char *p = string;
	for (i=0; i<len_in; i++, p++) {

		// check for end of existing variable
		if (open != -1) {
			if (*p != '}') continue;
			
			match_t *el = malloc(sizeof(match_t));
			el->next = NULL;
			el->value = NULL;
			el->start  = open;
			el->length = i - open;
			el->name = malloc(el->length-1);
			memcpy(el->name, string+open+2, el->length-2);
			el->name[el->length-2] = 0;

			// attach and move tail
			last->next = el;
			last = el;

			// reset state
			open = -1;
			count++;
			continue;
		}

		// check for beginning of new variable
		if (*p == '$') {
			res->found++;
			if (*(p+1) == '{') {
				open = i;
			}
		}
	}

	// brackets unclosed, not recoverable
	if (open != -1) {
		res->error = 1;
		return res;
	}

	// no placeholder found
	if (count == 0) {
		res->result = strdup(string);
		return res;
	}

	// 2. find all environment variables
	len_out += len_in;
	match_t *current = root->next;
	while (current != NULL) {
#ifdef DEBUG
		printf("[%d:%d] %s\n", current->start, current->length, current->name);
#endif
		current->value = getenv(current->name);

		if (current->value != NULL) {
			res->replaced++;
			len_out += strlen(current->value);
		} else {
			res->unreplaced++;
		}

		current = current->next;
	}

	// 4. generate output string
	res->result = malloc(len_out+1);
	res->result[0] = 0;
	int consumed = 0;
	char *cursor = string;
	current = root->next;
	while (current != NULL) {
		int l = current->start-consumed;
		char *chunk = malloc(l + 1);
		memcpy(chunk, cursor, l);
		chunk[l] = 0;
		cursor += l + current->length+1;
		consumed += l + current->length+1;

		strcat(res->result, chunk);
		if (current->value != NULL)
			strcat(res->result, current->value);
		free(chunk);

		// move pointers
		current = current->next;
	}

#ifdef DEBUG
	printf("res: '%s'\n", res->result);
#endif

	return res;
}

void logger(const char* tag, const char* message) {
	time_t now;
	time(&now);
	char *ctime_no_newline = strtok(ctime(&now), "\n");
	if (logfp != NULL)
		fprintf(logfp, "%s [%s]: %s\n", ctime_no_newline, tag, message);
	else
		fprintf(stdout, "%s [%s]: %s\n", ctime_no_newline, tag, message);
}