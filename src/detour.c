#include "util.h"

int main(int argc, char **argv) {
	int i=0;

	// check user input. We expect at least 1 parameter
	if (argc < 2) {
		error("Error: Too few Parameters\n");
		return 1;
	}

	// open log file
	char *logf = open_log();
	if (logf == NULL) {
		fprintf(stderr, "Failed to open log file\n");
		exit(127);
	}

	// debug output, show all parameters
#ifdef DEBUG
	for (i=0; i<argc; i++) {
		printf("%d: %s\n", i, argv[i]);
	}
#endif
	// try to resolve our location with realpath
	char location[PATH_MAX] = {0};
	int ok;

#ifdef __MINGW32__
	ok = (_fullpath(location, argv[0], PATH_MAX) != NULL);
#else
	ok = (realpath(argv[0], location) != NULL);
#endif
	if (!ok) return 2;
	
	fwdslash(location);
	char config[PATH_MAX] = {0};
	strcpy(config, location);
	dirname(location);

	logger("LOCATION", location);

#ifdef DEBUG
	printf("executable folder: %s\n", location);
#endif

	// config file location
	int l = strlen(config);
	config[l-3] = 'i'; config[l-2] = 'n'; config[l-1] = 'i';
#ifdef DEBUG
	printf("config file : %s\n", config);
#endif

	logger("CONFIG_FILE", config);

	// parse ini file
	ini_section_list_t *ini = read_ini(config);
	if (ini == NULL) {
		error("Error: Failed to parse config file.\n");
		return 3;
	}

	// find specific item
	int start_param = 1;
	char *section = strdup(argv[1]);
	#ifdef PROTOHAND_FLAVOUR
	logger("MODE", "Running as ph:// mode");
	start_param = 0;
	for (i=0; i<strlen(section); i++) {
		if (section[i] == ':') {
			section[i] = 0;
			break;
		}
	}
	logger("SECTION", section);

	#endif

	ini_section_t *sect = ini_find_section(ini, section);

	if (sect == NULL) {
		char *msg = malloc(strlen(argv[1])+100);
		sprintf(msg, "Error: Config '%s' does not exist.\n", section);
		error(msg);
		return 4;
	}

	char *cmd = NULL;

	if (sect != NULL) {
#ifdef DEBUG
		printf("Section name: %s\n", sect->name);
#endif

		ini_item_t *kv = ini_find_key(sect, "exe");
		if (kv != NULL) {
			envreplace_t *ret = subst_env_vars(kv->value);
			cmd = ret->result;
#ifdef DEBUG
			printf("-----> '%s': '%s'\n", kv->name, cmd);
#endif
			logger("CMD", cmd);
		}
	}
	//printf("argc: %d\n", argc); return 0;


	// build new command line
	char **args = malloc(sizeof(char*) * argc+2);
	args[0] = cmd;
	int o=1; // skip first param in normal mode
	i=start_param+1;
	for (; i<=argc; i++) {
		args[o++] = argv[i];
	}
	args[o-1] = NULL;
	//printf("argv[1]: %s, o=%d, i=%d\n", argv[1], o, i); //return 0;
	//printf("args: %d\n", o); return 0;

	i=0;
	while(args[i] != NULL) {
#ifdef DEBUG
		printf("> %d: %s\n", i, args[i]);
#endif
		i++;
	}

	// execute program
	run(args, sect);

	// prune log file to 5mb
	prune_log_file(1024*5000);

	return 0;
}