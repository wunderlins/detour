# detour

## About

`Detour` is a stub which launches the real exe. The stub is here to prevent 
locks during updates to the real executable by providing a mechanism to 
configure dirrerent versions.

## Problem to solve 

This program is born out of the need to have a central mechanism to 
configure which executable is run.

Problems arise if in a windows environment executables are run 
from network (CIFS) shares. Several services (Anti-virus, Cortex or 
equivalent. etc.) try to get locks on the file. Therefore it is
inherently hard to replace / uprade an executable and it's assets.

`detour` is a stub that just run a different application configured in 
`detour.ini`. Changing the location in the config file will then launch 
a different version (for example) upon the next request. This makes 
it painless to switch application versions for example.

## Configuration

The configuraion file name must be identical to the executables file 
name but ending in `*.ini`. 

```ini
;; multiple entries are allowd. The section name is expected as 
;; parameter 1 (argv[1]) on the command line.
;;
;; you can use environment variables in paths, names are case sensitive and
;; the syntax is as follows: ${varname}. Undefined variables will be 
;; replaced with '' (empty string).
;;
;; 2022, Simon Wunderlin, v. 1.x
;;
[cmd]
; Required: FQ Path to executable to run
exe = c:\windows\system32\cmd.exe
; Optional: change working directory before executing
; Default: ""
cwd = ${windir}
; Optional: hide cmd window if there is any
; Default: False
hidden = False
```

To run the above configuration you would run:
```cmd
c:\> detour.exe cmd /c calc.exe
```

This will use the configuration `cmd` and pass all other parameters 
after `cmd` to the command being executed. This will result in the 
following command line:

```cmd
c:\windows\system32\cmd.exe /c calc.exe
```

# Development

`Detour` is build with cross platform in mined. However it is built to solve 
problems on the windows platform primarely. Therefore some of the code 
still needs a posix implementation.

## Build environment

```bash
git clone git@gitlab.com:wunderlins/detour.git
cd detour
git submodule update --init
```

## Build

### prereqesites

#### Debian

```bash
apt install cmake build-essential # for and command line tools
apt install cunit # for unit tests
apt install doxygen texlive-font-utils # for api documentation
```

#### Mingw64

```bash
pacman -S mingw64/mingw-w64-x86_64-cmake \
  mingw64/mingw-w64-x86_64-cunit \
  mingw64/mingw-w64-x86_64-doxygen 
```

## Test

```bash
cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug
# OR protohand specific version
cmake -S . -B build -DCMAKE_BUILD_TYPE=Debug -DPROTOHAND_FLAVOUR=On
cmake --build build
ctest --test-dir build
```

### Generic version

```bash
cmake -S . -B build -DCMAKE_BUILD_TYPE=Release
cmake --build build
```

### Protohand Version

```bash
cmake -S . -B build -DCMAKE_BUILD_TYPE=Release -DPROTOHAND_FLAVOUR=On
cmake --build build
```

The executabe and an example configu file can be found in `build/detour.exe` 
and `build/detour.ini`.